libfile-fnmatch-perl (0.02-3) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Nathan Handler from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 30 Jun 2022 23:51:13 +0100

libfile-fnmatch-perl (0.02-2) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Nathan Handler ]
  * Email change: Nathan Handler -> nhandler@debian.org

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Switch to "3.0 (quilt)" source format.
  * debian/rules: convert to three-line dh(1) variant.
  * debian/copyright: refresh license stanzas.
  * Use debhelper 9.20120312 to get all hardening flags.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sat, 22 Feb 2014 17:42:35 +0100

libfile-fnmatch-perl (0.02-1) unstable; urgency=low

  * Initial Release. (closes: #535235) (LP: #220477)

 -- Nathan Handler <nhandler@ubuntu.com>  Wed, 01 Jul 2009 22:25:11 +0000
